# CHANGELOG

The version labels below refer to the git tags and the associated container image tags built from those git commits. Reproducibility is not completely guaranteed because some dependencies are not immutable (e.g. system packages installed via `apt`).

## compose-v0.1.0

Added initial version of the CompOSE library for internal use.

## porter-v0.1.0

Added initial version of the Porter library for internal use.

## 1.1.0

Added the `yaml-cpp_wrapper` library.

## 1.0.0

Initial static release
