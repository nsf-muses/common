# Motivation

The CompStar Online Supernovae Equations of State (CompOSE) is a repository for equations of state (EoS) for use in astrophysical applications. Providers can submit EoS in a common format containing thermodynamic, microscopic, compositional and astrophysical data. Users can then use these equations of state in their own simulations like neutron-star merger and supernova simulations. This resource can also been used in large-scale analyses of neutron star equations of state. This script takes output from various modules of the MUSES framework and converts it into the common format required for submission to CompOSE.

# Code Overview

This Python script first reads in output from a MUSES module in to a DataFrame. It then determines the dimensionality of this table. With this information, the code then creates a regular grid for which to interpolate on with recommended CompOSE sizing. Then using griddata from scipy.interpolate, we interpolate points on the grid for various quantities required by CompOSE. This uses the Delauney triangulation method. Afterwards we output tables in CompOSE format. These tables are eos.t, eos.nb, eos.thermo, eos.yq, eos.compo and eos.micro.

# Local installation

Use pip to install the `compose` package locally:

```bash
pip install .
```

# Container-based test and development

Use Docker Compose to build and run the development test scripts in a container:

```bash
docker compose up --build
```
