import pandas as pd
import numpy as np
from scipy.interpolate import griddata
from muses_porter import Porter
import os
import yaml

class Interpolator:
	def __init__(self):
		self.thermo = pd.DataFrame()
		self.particles = pd.DataFrame()
		self.micro = pd.DataFrame()
		self.dimension = 0
		self.fileName = "eos.csv"
		self.interpolated = False
		self.varNames = []
		self.spacings = []
		self.pts = []
		self.mins = []
		self.maxs = []

	def set_thermo(self, filename, has_header = False, add_header = "None"):
		if(add_header != "None"):
			self.thermo = pd.read_csv(filename, names = add_header, delimiter=",")
			
		elif not(has_header):
			f = open(filename, "r")
			array = f.readline()
			f.close()
			array = array.split(",")
			length = len(array)
			names = ['temperature','muB','muS','muQ','baryon_density','strange_density','charge_density','energy_density','pressure','entropy']
			if(len(names)<length):
				for i in range(0,length-len(names)):
					names.append('var'+str(i))				
			self.thermo = pd.read_csv(filename, names = names, delimiter =",")
			
			
		else:
			self.thermo = pd.read_csv(filename,delimiter = ",")
		
		
	def set_particles(self, filename):
		self.particles = pd.read_csv(filename, delimiter=",")

	def set_micro(self, filename):
		self.micro = pd.read_csv(filename, delimiter=",")

	def set_dimension(self, dimension):
		self.dimension = dimension
	
	def set_fileName(self, filename):
		self.fileName = filename

	def set_interpolated(self, interpolated):
		self.interpolated = interpolated
	
	def set_varNames(self, varNames):
		self.varNames = varNames
		
	def set_spacings(self, spacings):
		self.spacings = spacings
		
	def set_pts(self,pts):
		self.pts = pts
		
	def set_mins(self,mins):
		self.mins = mins
	
	def set_maxs(self,maxs):
		self.maxs = maxs

	def get_thermo(self):
		return self.thermo

	def get_particles(self):
		return self.particles

	def get_dimension(self):
		return self.dimension

	def get_interpolated(self):
		return self.interpolated

	def get_micro(self):
		return self.micro
		
	def set_default_mins_maxs(self):
		for i in self.varNames:
			if(i == 'baryon_density'):
				self.mins.append(self.thermo['baryon_density'].min())
				self.maxs.append(self.thermo['baryon_density'].max())
	
			if(i == 'charge_fraction'):
				if(i in self.thermo):
					yq = self.thermo['charge_fraction']
				else:
					yq = self.thermo['charge_density']/self.thermo['baryon_density']

				self.mins.append(yq.min())
				self.maxs.append(yq.max())
				
			
			if(i == 'strange_fraction'):
				if(i in self.thermo):
					ys = self.thermo['strange_fraction']
				else:
					ys = self.thermo['strange_density']/self.thermo['strange_density']

				self.mins.append(ys.min())
				self.maxs.append(ys.max())
				
			
			if(i == 'temperature'):
				self.mins.append(self.thermo['temperature'].min())
				self.maxs.append(self.thermo['temperature'].max())
				
				
	def set_default_grid(self,varNames,pts = 100, spacings = 'linear'):
		self.varNames = varNames
		self.set_default_mins_maxs()
		
		pts = []
		spacings = []
		for i in self.varNames:
			pts.append(pts)
			spacings.append(spacing)
			
		self.pts = pts
		self.spacings = spacings
		
	def read_config(self, filename):
		with open(filename, 'r') as file:
			# Parse the YAML data
			parsed_data = yaml.safe_load(file)
			
		thermo_filename = os.path.join('input', parsed_data['input_files']['thermo_file_name'])
		print(f'''Loading thermo file: "{thermo_filename}"...''')
		self.set_thermo(thermo_filename)
		self.set_fileName(parsed_data['input_files']['thermo_file_name'])
		
		if(parsed_data['input_files']['compo_file_name'] is not None):
			compo_filename = os.path.join('input', parsed_data['input_files']['compo_file_name'])
			print(f'''Loading particle file: "{compo_filename}"...''')
			self.set_particles(compo_filename)

	
		var_names = []
		var_spacings = []
		var_mins = []
		var_maxs = []
		var_points = []
	
		if(parsed_data['variables']['density']):
			self.varNames.append('baryon_density')
			self.spacings.append(parsed_data['grid_spacing']['density_spacing'])
			self.mins.append(parsed_data['grid_spacing']['density_min'])
			self.maxs.append(parsed_data['grid_spacing']['density_max'])
			self.pts.append(parsed_data['grid_spacing']['density_pts'])
		if(parsed_data['variables']['charge_fraction']):
			self.varNames.append('charge_fraction')
			self.spacings.append(parsed_data['grid_spacing']['charge_fraction_spacing'])
			self.mins.append(parsed_data['grid_spacing']['charge_fraction_min'])
			self.maxs.append(parsed_data['grid_spacing']['charge_fraction_max'])
			self.pts.append(parsed_data['grid_spacing']['charge_fraction_pts'])
		if(parsed_data['variables']['strange_fraction']):
			self.varNames.append('strange_fraction')
			self.spacings.append(parsed_data['grid_spacing']['strange_fraction_spacing'])
			self.mins.append(parsed_data['grid_spacing']['strange_fraction_min'])
			self.maxs.append(parsed_data['grid_spacing']['strange_fraction_max'])
			self.pts.append(parsed_data['grid_spacing']['strange_fraction_pts'])
		if(parsed_data['variables']['temperature']):
			self.varNames.append('temperature')
			self.spacings.append(parsed_data['grid_spacing']['temperature_spacing'])
			self.mins.append(parsed_data['grid_spacing']['temperature_min'])
			self.maxs.append(parsed_data['grid_spacing']['temperature_max'])
			self.pts.append(parsed_data['grid_spacing']['temperature_pts'])
			
		return
			

	def make_grid(self):
		self.set_dimension(len(self.varNames))
		
		var_values = []
		pts = []
		grid = []
		
		for i in self.varNames:
			if(i == 'baryon_density'):
				nb = self.thermo['baryon_density']
				var_values.append(nb)
			if(i == 'charge_fraction'):
				yq = self.thermo['charge_density']/self.thermo['baryon_density']
				var_values.append(yq)
			if(i == 'strange_fraction'):
				ys = self.thermo['strange_density']/self.thermo['baryon_density']
				var_values.append(ys)
			if(i == 'temperature'):
				t = self.thermo['temperature']
				var_values.append(t)
			
		if(len(self.varNames) == 1):
			if(self.spacings[0] == 'log'):
				grid = np.geomspace(self.mins[0], self.maxs[0], self.pts[0])
			else:
				grid = np.linspace(self.mins[0], self.maxs[0], self.pts[0])
				
			for i in range(len(var_values[0])):
				pts.append(var_values[0][i])
					
		elif(len(self.varNames) == 2):
			if(self.spacings[0] == 'log'):
				var1i = np.geomspace(self.mins[0], self.maxs[0], self.pts[0])
			else:
				var1i = np.linspace(self.mins[0], self.maxs[0], self.pts[0])
			if(self.spacings[1] == 'log'):
				var2i = np.geomspace(self.mins[1], self.maxs[1], self.pts[1])
			else:
				var2i = np.linspace(self.mins[1], self.maxs[1], self.pts[1])
			
			for j in range(len(var1i)):
				for k in range(len(var2i)):
					grid.append([var1i[j],var2i[k]])

			for i in range(len(var_values[0])):
				pts.append([var_values[0][i],var_values[1][i]])


		elif(len(self.varNames) == 3):
			if(self.spacings[0] == 'log'):
				var1i = np.geomspace(self.mins[0], self.maxs[0], self.pts[0])
			else:
				var1i = np.linspace(self.mins[0], self.maxs[0], self.pts[0])
			if(self.spacings[1] == 'log'):
				var2i = np.geomspace(self.mins[1], self.maxs[1], self.pts[1])
			else:
				var2i = np.linspace(self.mins[1], self.maxs[1], self.pts[1])
			if(self.spacings[2] == 'log'):
				var3i = np.geomspace(self.mins[2], self.maxs[2], self.pts[2])
			else:
				var3i = np.linspace(self.mins[2], self.maxs[2], self.pts[2])

			
			for j in range(len(var1i)):
				for k in range(len(var2i)):
					for l in range(len(var3i)):
						grid.append([var1i[j],var2i[k],var3i[l]])
						
			for i in range(len(var_values[0])):
				pts.append([var_values[0][i],var_values[1][i],var_values[2][i]])
				
		elif(len(self.varNames) == 4):
			if(self.spacings[0] == 'log'):
				var1i = np.geomspace(self.mins[0], self.maxs[0], self.pts[0])
			else:
				var1i = np.linspace(self.mins[0], self.maxs[0], self.pts[0])
			if(self.spacings[1] == 'log'):
				var2i = np.geomspace(self.mins[1], self.maxs[1], self.pts[1])
			else:
				var2i = np.linspace(self.mins[1], self.maxs[1], self.pts[1])
			if(self.spacings[2] == 'log'):
				var3i = np.geomspace(self.mins[2], self.maxs[2], self.pts[2])
			else:
				var3i = np.linspace(self.mins[2], self.maxs[2], self.pts[2])
			if(self.spacings[3] == 'log'):
				var4i = np.geomspace(self.mins[3], self.maxs[3], self.pts[3])
			else:
				var4i = np.linspace(self.mins[3], self.maxs[3], self.pts[3])

			for j in range(len(var1i)):
				for k in range(len(var2i)):
					for l in range(len(var3i)):
						for m in range(len(var4i)):
							grid.append([var1i[j],var2i[k],var3i[l],var4i[m]])

			for i in range(len(var_values[0])):
				pts.append([var_values[0][i],var_values[1][i],var_values[2][i],var_values[3][i]])
				
		return [pts, grid]
		
			
	def interpolate(self):
		
		grid = self.make_grid()
		newthermo = pd.DataFrame()
		newparticles = pd.DataFrame()
		
		#replace columns in self.thermo and self.particles with interpolated columns
		for (columnName, columnData) in self.thermo.items():
			if(columnName not in self.varNames):
				newthermo[columnName]= griddata(grid[0],columnData,grid[1])
			else:
				if(len(self.varNames) == 1):
					newthermo[columnName] = grid[1]
				else:
					columnNumber = self.varNames.index(columnName)
					newcolumn = []
					for i in range(0,len(grid[1])):
						newcolumn.append(grid[1][i][columnNumber])
					newthermo[columnName] = newcolumn
					
		newcolumn = []
		newcolumn = grid[1]
		columnName = self.varNames
		if(len(self.varNames) == 1):
			newcolumn = np.reshape(newcolumn,[len(grid[1]),1])
		newthermo[columnName] = newcolumn
										
		if not((self.particles.empty)):
			newparticles[columnName] = newcolumn
			for (columnName, columnData) in self.particles.items():
				if(columnName not in self.varNames):
					newparticles[columnName]= griddata(grid[0],columnData,grid[1])
		
		self.thermo = newthermo
		self.particles = newparticles
		self.interpolated = True
		
	def output_thermo(self,fileName):
		try:
			self.thermo.to_csv(fileName,',','NaN', index = False,float_format = '%.7e' )
		
		except OSError:
			os.makedirs(os.path.dirname(fileName))
			self.thermo.to_csv(fileName,',','NaN', index = False,float_format = '%.7e' )
							
	def output_compo(self,fileName):
		try:
			self.particles.to_csv(fileName,',','NaN', index = False,float_format = '%.7e' )
		
		except OSError:
			os.makedirs(os.path.dirname(fileName))
			self.particles.to_csv(fileName,',','NaN', index = False,float_format = '%.7e' )

class Compose(Interpolator):
	def __init__(self):
		super().__init__()

	def set_default_compose_grid(self,varNames):
		self.varNames = varNames
		self.set_default_mins_maxs()
		for i in self.varNames:
			if(i == 'baryon_density'):
				self.pts.append(301)
				self.spacings.append('log')
				
			if(i == 'charge_fraction'):
				self.pts.append(60)
				self.spacings.append('linear')
			
			if(i == 'strange_fraction'):
				self.pts.append(60)
				self.spacings.append('linear')
			
			if(i == 'temperature'):
				self.pts.append(81)
				self.spacings.append('linear')
				
	def compose_output_thermo(self, fileName):
		if not self.interpolated:
			print("Not interpolated data")
			return
		
		possible_columns = ['temperature', 'baryon_density','charge_fraction', 'pressure', 'entropy', 'muB','muQ','muL','free_energy','energy_density']
		output_array = []

		column_length = len(self.thermo[self.thermo.columns[0]])

		for i in possible_columns:
			if(i in self.thermo.columns):
				output_array.append(self.thermo[i])
	
			else:
				output_array.append(np.zeros(column_length))
		
		mn = 938.9187125
		mp = 938.9187125

		
		output_array[3] = output_array[3]/output_array[1]
		output_array[4] = output_array[4]/output_array[1]
		output_array[5] = output_array[5]/mn - 1
		output_array[6] = output_array[6]/mn
		output_array[7] = output_array[7]/mn
		output_array[8] = output_array[8] / (output_array[1] * mn) - 1
		output_array[9] = output_array[9] / (output_array[1] * mn) - 1

		for i in self.thermo.columns:
			if(i not in possible_columns):
				output_array.append(self.thermo[i])
		
		#checks if eos has leptons
		has_leptons = 1 if not np.all(output_array[7]) else 0

	
		#calculates free energy if not given
		if np.all(output_array[8]):
			output_array[8] = np.where(output_array[0] != 0, output_array[9] - output_array[0] * output_array[4], output_array[9])

		
		#finds unique values of nb,yq,T       
		nbc = np.unique(output_array[1])
		Tc = np.unique(output_array[0])
		
		if('charge_fraction' in self.varNames):
			yqc = np.unique(output_array[2])
		else:
			yqc = [0]
		if('Y_S' in self.varNames):
			ysc = np.unique(output_array[len(output_array)-1])
		else:
			ysc = [0]
		
		for i in range(len(Tc)):
			if(np.isnan(Tc[i])):
				Tc = np.delete(Tc,i)
				
		for i in yqc:
			if (np.isnan(i)):
				yqc = np.delete(yqc,i)
					
		for i in nbc:
			if (np.isnan(i)):
				nbc = np.delete(nbc,[i])
				
		for i in range(len(ysc)):
			if (np.isnan(i)):
				ysc = np.delete(ysc,[i])
				
		try:
			# Write eos.nb file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.nb"),nbc, fmt='%.7e', header="1\n{}".format(len(nbc)), comments='')

			# Write eos.yq file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.yq"), yqc, fmt='%.7e', header="1\n{}".format(len(yqc)), comments='')

			# Write eos.t file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.t"), Tc, fmt='%.7e', header="1\n{}".format(len(Tc)), comments='')

		except OSError:
			os.makedirs(os.path.dirname(fileName))
			# Write eos.nb file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.nb"),nbc, fmt='%.7e', header="1\n{}".format(len(nbc)), comments='')

			# Write eos.yq file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.yq"), yqc, fmt='%.7e', header="1\n{}".format(len(yqc)), comments='')

			# Write eos.t file
			np.savetxt(os.path.join(os.path.dirname(fileName), "eos.t"), Tc, fmt='%.7e', header="1\n{}".format(len(Tc)), comments='')




		#write eos.thermo
		f = open(os.path.join(os.path.dirname(fileName), "eos.thermo"), "w")
		f.write(str(mn) + "  " + str(mp) + "  " + str(has_leptons)+ "\n")
		count = 0 
		for i in range(len(Tc)):
			for j in range(len(nbc)):
				for k in range(len(yqc)):
					for l in range(len(ysc)):
						f.write(
						  str(i+1).rjust(4) + "  "
						  + str(j+1).rjust(4) + "  "
						  + str(k+1).rjust(4) + "  "
						  + str("{:.7e}".format(output_array[3][count])) + "  "
						  + str("{:.7e}".format(output_array[4][count])) + "  "
						  + str("{:.7e}".format(output_array[5][count])) + "  "
						  + str("{:.7e}".format(output_array[6][count])) + "  "
						  + str("{:.7e}".format(output_array[7][count])) + "  "
						  + str("{:.7e}".format(output_array[8][count])) + "  "
						  + str("{:.7e}".format(output_array[9][count])) + "  "
						  + str(len(output_array) - 10) + "  ")
						for m in range(10,len(output_array)):
							f.write(str("{:.7e}".format(output_array[m][count]))+ "  ")
				
						f.write("\n")
						count= count +1

		f.close()


			
	def compose_output_compo(self, base_dir='.'):
		if not self.interpolated:
			print("Not interpolated data")
			return
	
		possible_columns = ['temperature', 'nB', 'Y_Q', 
							'proton_nB','neutron_nB', 
							'lambda_nB', 'sigma+_nB', 
							'sigma0_nB', 'sigma-_nB', 
							'xi0_nB', 'xi-nB', 'delta++_nB', 
							'delta+_nB', 'delta0_nB', 'delta-_nB', 
							'sigma+*_nB', 'sigma0*_nB', 'sigma-*_nB',
							'xi0*_nB', 'xi-*_nB', 'omega-_nB', 
							'up_nB', 'down_nB', 'strange_nB',
							'electron_density', 'muon_density']
		particle_indices = [1,1,1,10,11,100,112,111,110,121,120,23,22,21,20,1112,1111,1110,1121,1120,1119,500,501,502,0,1]
		output_array = []

		m = 0
		while(m < len(possible_columns)):
			if(possible_columns[m] in self.particles.columns):
					output_array.append(self.particles[possible_columns[m]])
					m += 1

			else:
				del particle_indices[m]
				del possible_columns[m]


		for i in range(len(output_array)):
			if(i>2):
				output_array[i] = [x/y for x,y in zip(output_array[i],output_array[1])]
	
		#finds unique values of nb,yq,T        
		nbc = np.unique(output_array[1])
		yqc = np.unique(output_array[2])
		Tc = np.unique(output_array[0])
		ysc = np.unique(output_array[len(output_array)-1])
		
		for i in range(len(Tc)):
			if(np.isnan(Tc[i])):
				np.delete(Tc,i)
				
		for i in yqc:
			if (np.isnan(i)):
				np.delete(yqc,i)
					
		for i in nbc:
			if (np.isnan(i)):
				np.delete(nbc,[i])
				
		for i in ysc:
			if (np.isnan(i)):
				ysc = np.delete(ysc,[i])

		#write eos.compo file
		f1 = open(os.path.join(base_dir, "eos.compo"),"w")

		count = 0
		for i in range(len(Tc)):
			for j in range(len(nbc)):
				for k in range(len(yqc)):
					for l in range(len(ysc)):
						f1.write(
							str(i+1).rjust(4) + " "
							 + str(j+1).rjust(4) + "  "
							 + str(k+1).rjust(4) + "  "
							 + "1" + "  "
							 + str(len(particle_indices)) + "  ")
						for m in range(len(output_array)):
							if(m == len(output_array)-1):
								f1.write(str(particle_indices[l]) + "  " + str("{:.7e}".format(output_array[l][count])) + "  0"+ "\n")

							else:
								f1.write(str(particle_indices[l]) + "  " + str("{:.7e}".format(output_array[l][count])) + "  ")
						count = count + 1

		f1.close()

