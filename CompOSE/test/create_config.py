#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create a YAML configuration file for Syntheiss based on user input
# Determines the necessary fields in config.yaml based on the OpenAPI specification

import argparse
import os
import yaml
import sys
from openapi_core import Spec

# Default paths
DEFAULT_API_FILE_PATH = os.path.join(os.path.dirname(__file__), 'api/OpenAPI_Specifications_CompOSE.yaml')
DEFAULT_CONFIG_FILE_PATH = os.path.join(os.path.dirname(__file__), 'input')
DEFAULT_CONFIG_FILE= "config.yaml"

def main():
    
    # Create command line argument parser
    parser = argparse.ArgumentParser(
        description="Create a YAML configuration file for Chiral EFT module based on user input conforming to the OpenAPI specification"
    )

    # Paths
    parser.add_argument(
        "--api_file_path",
        type=str,
        default=DEFAULT_API_FILE_PATH,
        help="Path to the OpenAPI specification file",
    )

    parser.add_argument(
        "--config_file_path",
        type=str,
        default=DEFAULT_CONFIG_FILE_PATH,
        help="Path to the created configuration file",
    )

    (args, rest) = parser.parse_known_args()

    # Parse command line arguments based on OpenAPI spec
    args = parse_args_from_spec(parser, spec_file_path=args.api_file_path)

    # Write parsed command line arguments to YAML configuration file based on OpenAPI spec
    write_args_to_config(args, spec_file_path=args.api_file_path, config_file_path=os.path.join(args.config_file_path, DEFAULT_CONFIG_FILE))


def parse_args_from_spec(argparser, spec_file_path):
    
    # Reading OpenAPI specifications
    with open(spec_file_path, 'r') as fp:
        openapi_specs = Spec.from_file(fp)
    
    # Get schema for config.yaml file
    config_path = openapi_specs / "components" / "schemas" / "config" / "properties"

    # Recursively iterate through nested schema and create a command line argument for each property
    def add_arguments(node, required=[]):
        for prop, value in node.items():
            argument_exists = any(action.dest == prop for action in argparser._actions)

            if value['type'] == 'object':
                add_arguments(value['properties'], required=value['required'] if 'required' in value else [])
            elif not argument_exists:                
                argparser.add_argument(
                    "--" +prop,
                    type=str,
                    required=prop in required,
                    help=value['description'],
                    nargs='*',

                )

    with config_path.open() as config:
        add_arguments(config)
        
    # Parse command line arguments
    return argparser.parse_args()


def write_args_to_config(args, spec_file_path, config_file_path):

    # Reading OpenAPI specifications
    with open(spec_file_path, 'r') as fp:
        openapi_specs = Spec.from_file(fp)
    
    # Get schema for config.yaml file
    config_path = openapi_specs / "components" / "schemas" / "config" / "properties"

    # Recursively append arguments to dictionary based on OpenAPI schema and cast argument types as they are placed    
    def append_arguments(node, data, path='', indexes=None, required=None):
        # Initialize the indexes dictionary on the first call
        if indexes is None:
            indexes = {}
        
        if required is None:
            required = []

        for prop, value in node.items():
            current_path = f"{path}.{prop}" if path else prop


            if value['type'] == 'object':
                required= value.get('required', None) 

                data[prop] = {}
                append_arguments(value['properties'], data[prop], current_path, indexes, required)
            else:
                arg_values = vars(args).get(prop, None)
    
                if prop in required and (arg_values is None or (isinstance(arg_values, list) and not arg_values)) :
                    sys.exit(f"Error: Required property '{prop}' is missing.")
                
                selected_value = None

                if arg_values:
                    selected_value = None
                    if isinstance(arg_values, list):
                        
                        prop_index = indexes.get(prop, 0)
                        
                        if prop_index < len(arg_values):
                            selected_value = arg_values[prop_index]
                            indexes[prop] = prop_index + 1  # Update the index
                        else:
                            # Use the default value if available, when more nodes than values
                            if prop in required :
                                sys.exit(f"Error: Required property '{prop}' is missing.")
                            selected_value = None
                    else:
                        # For non-list arguments or lists with a single value
                        selected_value = arg_values if arg_values is not None else value.get('default', None)

                    if selected_value is not None:
                        data[prop] = type_cast(selected_value, value['type'])

    data = {}
    with config_path.open() as config:
        append_arguments(config, data)
    
    # Write configuration data to config.yaml file
    with open(config_file_path, 'w') as fp:
        yaml.safe_dump(data, fp)


def type_cast(value, type_name):
    if type_name == 'number':
        return float(value)
    elif type_name == 'integer':
        return int(value)
    elif type_name == 'boolean':
        if value.lower() in ("true", "1", "t", "y", "yes"):
            return True
        elif value.lower() in ("false", "0" , "f", "n", "no"):
            return False
    else:
        return value


if __name__ == "__main__":
    print("\nStarting execution of create_config.py...")
    main()
    print("\nFinished create_config.py...")
