if __name__ == "__main__":
	import sys
	import os
	from filecmp import cmp

	# Add the "compose" package directory to the Python path and import the class
	sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), '..')))
	from compose import Compose
	compose = Compose()

	
	print(f'''Interpolating...''')
	compose.read_config(filename = 'input/config.yaml')
	compose.interpolate()

	print(f'''Generating output files...''')

	#thermo_output_base_dir = 
	thermo_output_filename = os.path.join('output', compose.fileName)
	compose.output_thermo(thermo_output_filename)

	compose.output_thermo(thermo_output_filename)

	compose.compose_output_thermo(thermo_output_filename)
	
	#if not(compose.particles.empty):
		#compose.output_compo(thermo_output_filename)

	# print(f'''Validating output files...''')
	# invalid_files = [filename for filename in [
	#     "eos.nb",
	#     "eos.t",
	#     "eos.thermo",
	#     "eos.yq",
	#     "eos.compo",
	# ] if not cmp(f"/opt/test/output/{filename}", f"/tmp/{filename}")]
	# if invalid_files:
	#     err_msg = f'''Files failed validation: {', '.join(invalid_files)}'''
	#     raise Exception(err_msg)

