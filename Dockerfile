FROM ubuntu:20.04 as build

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        g++ \
        git \
        cmake \
        make \
        ca-certificates \
        wget \
        libgsl-dev \
    && rm -rf /var/lib/apt/lists/*

## Install Boost
## ref: https://www.boost.org/doc/libs/1_67_0/more/getting_started/unix-variants.html
RUN mkdir -p /tmp/boost/ && \
    cd /tmp/boost && \
    wget https://boostorg.jfrog.io/artifactory/main/release/1.67.0/source/boost_1_67_0.tar.gz && \
    tar zxf boost_1_67_0.tar.gz && \
    rm boost_1_67_0.tar.gz && \
    cd /tmp/boost/boost_1_67_0 && \
    ./bootstrap.sh sh --with-libraries=all --with-toolset=gcc && \
    ./b2 toolset=gcc && \
    ./b2 install && \
    rm -rf /tmp/boost


## Install yaml-cpp
## ref: https://github.com/jbeder/yaml-cpp
ARG YAML_BUILD_SHARED_LIBS="off"
RUN git clone https://github.com/jbeder/yaml-cpp /tmp/yaml-cpp
RUN mkdir /tmp/yaml-cpp/build
WORKDIR /tmp/yaml-cpp/build
RUN cmake -DYAML_BUILD_SHARED_LIBS=${YAML_BUILD_SHARED_LIBS} .. && \
    make && \
    make install && \
    rm -rf /tmp/yaml-cpp

## Install doctest
RUN git clone -b v2.4.8 https://github.com/doctest/doctest.git
RUN cd doctest && \
    ls -la && \
    cmake . && \
    make install

## Install Doxygen
## ref: https://www.how2shout.com/linux/how-to-install-doxygen-on-ubuntu-20-04-lts-forcal-fossa/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        doxygen  \
        doxygen-doc \
        doxygen-latex \
        doxygen-gui \
        graphviz

## Install the `yaml-cpp` wrapper library
COPY ./yaml-cpp_wrapper /tmp/yaml-cpp_wrapper
WORKDIR /tmp/yaml-cpp_wrapper
RUN make && PREFIX=/usr/local make install

FROM ubuntu:20.04

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        g++ \
        git \
        cmake \
        make \
        ca-certificates \
        wget \
        doxygen  \
        doxygen-doc \
        doxygen-latex \
        doxygen-gui \
        graphviz \
        libgsl-dev \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /usr/local/ /usr/local/

