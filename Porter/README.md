# Porter library for MUSES

This Python package is used by the MUSES framework to import and export data between MUSES modules.

## Installation

Install the porter library using `pip`:

```bash
pip install muses-porter
```

Run an example script by executing the following, checking `/tmp/` for generated output files:

```bash
python -m muses_porter.porter
```

## Development

Initialize a Python virtual environment like so:

```bash
$ python -m venv venv
$ source venv/bin/activate
(venv) 
$ 
```

Install the package:

```bash
(venv) 
$ pip install .
```

Run unit tests by executing:

```bash
python tests/TestPorter.py
```

Build and test in a Docker container:

```bash
$ docker build . -t muses-porter
$ docker run --rm -it muses-porter python -m muses_porter.porter
```
