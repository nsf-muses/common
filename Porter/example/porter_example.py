#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
main.py: A script demonstrating the usage of the Porter class for handling scientific data and metadata.
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
"""

from muses_porter import Porter


def main():
    """
    The main function demonstrates the usage of the Porter class to set, get, display, and export/import data and metadata.
    """

    # Create an instance of Porter
    porter = Porter()

    # Set data
    porter.set_data(
        {
            "temperature": [0, 0, 0, 0, 0],
            "mu_B": [1, 2, 3, 4, 5],
            "mu_S": [10, 20, 30, 40, 50],
            "mu_Q": [100, 200, 300, 400, None],
            "pressure": [1000, 2000, 3000, None, 5000],
            "energy_density": [10000, 20000, 30000, 40000, 50000],
            "entropy": [0, 0, 0, 0, 0],
        },
        {
            "temperature": ("MeV", float),
            "mu_B": ("MeV", float),
            "mu_S": ("MeV", float),
            "mu_Q": ("MeV", float),
            "pressure": ("MeV/fm3", float),
            "energy_density": ("MeV/fm3", float),
            "entropy": ("1/fm3", float),
        },
        dropna=True,
    )

    # Get data
    __ = porter.get_data()

    # Display data
    porter.display()

    # Subset of variables to keep in the exported file
    variables_to_keep = [
        "temperature",
        "mu_B",
        "mu_S",
        "mu_Q",
        "pressure",
        "energy_density",
    ]
    # Subset of units to keep in the exported file
    units_to_keep = ["MeV", "MeV", "MeV", "MeV", "MeV/fm3", "MeV/fm3"]
    # Subset of types to keep in the exported file
    types_to_keep = [float, float, float, float, float, float]

    print("\n\tExporting data to the CSV file...")

    # Export data to a CSV file with a subset of variables, units, and types
    porter.export_table(
        "/tmp/data.csv",
        extension="CSV",
        dropna=True,
        delimiter=",",
        verbose=True,
        float_format="%.12e",
        variables_to_keep=variables_to_keep,
    )

    # Reset the Porter instance to clear data and metadata
    porter.reset()

    print("\n\tImporting data from the CSV file...")

    # Import data from the CSV file knowing the header, units, and types
    porter.import_table(
        "/tmp/data.csv",
        extension="CSV",
        header=variables_to_keep,
        units=units_to_keep,
        types=types_to_keep,
        dropna=True,
        delimiter=",",
        verbose=True,
    )

    print("\n\tExporting data to the CSV file with metadata...")

    # Export data to a CSV file with a subset of variables, units, and types and metadata in a separate yaml file
    porter.export_table(
        "/tmp/data.csv",
        extension="CSV",
        filename_metadata="/tmp/metadata.yaml",
        dropna=True,
        delimiter=",",
        verbose=True,
        float_format="%.12e",
        variables_to_keep=variables_to_keep,
    )

    # Reset the Porter instance to clear data and metadata
    porter.reset()

    print("\n\tImporting data from the CSV file with metadata...")

    # Import data from the CSV file from yaml metadata
    porter.import_table(
        "/tmp/data.csv",
        extension="CSV",
        filename_metadata="/tmp/metadata.yaml",
        dropna=True,
        delimiter=",",
        verbose=True,
    )

    print("\n\tExporting data to the CSV file with metadata from openapi specs...")

    # Export data to a CSV file with a subset of variables, units, and types and metadata from openapi specs in a separate yaml file
    porter.export_table(
        "/tmp/data.csv",
        extension="CSV",
        filename_schema="OpenAPI_Specifications_Porter.yaml",
        schema="input_schema",
        filename_metadata="/tmp/metadata2.yaml",
        dropna=True,
        delimiter=",",
        verbose=True,
        float_format="%.12e",
    )

    # Reset the Porter instance to clear data and metadata
    porter.reset()

    print("\n\tImporting data from the CSV file with metadata from openapi specs...")

    # import data from the CSV file from openapi metadata
    porter.import_table(
        "/tmp/data.csv",
        extension="CSV",
        filename_schema="OpenAPI_Specifications_Porter.yaml",
        schema="output_schema",
        dropna=True,
        delimiter=",",
        verbose=True,
    )

    print("\n\tExporting data to the HDF5 file...")

    # Export data to an HDF5 file
    porter.export_table(
        "/tmp/data.h5",
        extension="HDF5",
        dropna=True,
        verbose=True,
        float_format="%.12e",
    )

    print("\n\tExporting data to the HDF5 file with metadata...")

    # Export data to an HDF5 file with metadata to a separate yaml file
    porter.export_table(
        "/tmp/data.h5",
        extension="HDF5",
        filename_metadata="/tmp/metadata3.yaml",
        dropna=True,
        verbose=True,
        float_format="%.12e",
    )

    # Reset the Porter instance to clear data and metadata
    porter.reset()

    print("\n\tImporting data from the HDF5 file...")

    # Import data from the HDF5 file
    porter.import_table(
        "/tmp/data.h5",
        extension="HDF5",
        dropna=True,
        verbose=True,
    )


if __name__ == "__main__":
    main()
