#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
porter.py: A module implementing the Porter class for handling scientific data and metadata.
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
"""

import pandas as pd
import numpy as np
import h5py
import yaml

from openapi_core import Spec


def numpy_dtype_to_yaml_type(dtype: type):
    """
    Map NumPy data types to YAML data types.

    Parameters:
    - dtype: NumPy data type.

    Returns:
    - YAML data type as a string.
    """
    if np.issubdtype(dtype, np.integer):
        return "integer"
    elif np.issubdtype(dtype, np.bool_):
        return "boolean"
    elif np.issubdtype(dtype, np.floating):
        return "number"
    elif np.issubdtype(dtype, np.unicode_) or np.issubdtype(dtype, str):
        return "string"
    else:
        return str(dtype)


def yaml_type_to_numpy_dtype(yaml_type: str):
    """
    Map YAML data types to NumPy data types.

    Parameters:
    - yaml_type: YAML data type as a string.

    Returns:
    - NumPy data type.
    """
    if yaml_type == "integer":
        return int
    elif yaml_type == "boolean":
        return bool
    elif yaml_type == "number":
        return float
    elif yaml_type == "string":
        return str
    else:
        raise ValueError(f"[porter error] YAML type {yaml_type} not supported.")


def read_yaml_metadata(filename: str):
    """
    Read metadata from a YAML file.

    Parameters:
    - filename (str): Path to the YAML file.

    Returns:
    - header (list): List of column names.
    - units (list): List of units for each column.
    - types (list): List of types for each column.
    """

    with open(filename, "r") as stream:
        try:
            metadata = yaml.safe_load(stream)
            header = list(metadata.keys())
            units = [metadata[col]["x-unit"] for col in header]
            types = [yaml_type_to_numpy_dtype(metadata[col]["type"]) for col in header]
        except yaml.YAMLError as exc:
            raise ValueError(f"[porter error] Error reading {filename}: {exc}")
            
    

    return header, units, types


def read_openapi_schema(filename: str, schema: str):
    """
    Read metadata from an OpenAPI schema.

    Parameters:
    - filename (str): Path to the OpenAPI file.
    - schema (str): Name of the schema.

    Returns:
    - header (list): List of column names.
    - units (list): List of units for each column.
    - types (list): List of types for each column.
    """

    # check file exists
    try:
        with open(filename, "r") as spec_file:
            pass
    except FileNotFoundError:
        raise FileNotFoundError(f"[porter error] File {filename} not found.")

    with open(filename, "r") as spec_file:
        openapi_specs = Spec.from_file(spec_file)

    # Extract header, units, and types for a specific schema
    if schema in openapi_specs["components"]["schemas"].keys():
        schema = openapi_specs["components"]["schemas"][schema]
        header = list(schema["items"]["properties"].keys())
        units = [schema["items"]["properties"][col].get("x-unit", "") for col in header]
        types = [
            yaml_type_to_numpy_dtype(schema["items"]["properties"][col].get("type", ""))
            for col in header
        ]
        return header, units, types
    else:
        raise ValueError(f"[porter error] Schema {schema} not found in {filename}.")


class Porter:
    """
    The Porter class handles scientific data, providing methods for setting, validating,
    and exporting/importing data, along with associated metadata, from/to various formats.
    """

    def __init__(self):
        """
        Initialize an instance of the Porter class with empty data, column metadata, and file metadata.
        """
        self.data = pd.DataFrame()
        self.column_metadata = {}

    def reset(self):
        """
        Reset the data, column metadata, and file metadata.
        """
        self.data = pd.DataFrame()
        self.column_metadata = {}

    def set_data(self, data: dict, column_metadata: dict, dropna=False):
        """
        Set the data and associated column metadata.

        Parameters:
        - data (dict): Dictionary representing the data.
        - column_metadata (dict): Dictionary representing metadata for each column.
        - dropna (bool): Flag to drop NaN values from the data (default is False).
        """
        assert self.validate_data(data)
        assert self.validate_column_metadata(column_metadata)
        assert self.validate_data_keys_vs_column_metadata_keys(data, column_metadata)

        self.data = pd.DataFrame(data).dropna() if dropna else pd.DataFrame(data)
        self.column_metadata = column_metadata

    def get_data(self):
        """
        Get the stored data.

        Returns:
        - pd.DataFrame: The stored data in DataFrame format.
        """
        return self.data

    def validate_data(self, data: dict):
        """
        Validate the format and content of the data.

        Parameters:
        - data (dict): Dictionary representing the data.

        Returns:
        - bool: True if the data is valid, False otherwise.
        """
        # check that data is a dictionary
        if not isinstance(data, dict):
            raise ValueError("[porter error] Data is not a dictionary.")

        # check that keys are strings
        if not all(isinstance(col, str) for col in data.keys()):
            raise ValueError("[porter error] Data keys are not strings.")

        # check that values are lists
        if not all(isinstance(val, list) for val in data.values()):
            raise ValueError("[porter error] Data values are not lists.")

        # check that every entry in the list is either a number or a string or a nan
        for val in data.values():
            if not all(isinstance(v, (int, float, str)) or pd.isna(v) for v in val):
                raise ValueError("[porter error] Data values are not numbers or strings.")

        # check that data is a valid DataFrame
        try:
            pd.DataFrame(data)
        except ValueError:
            raise ValueError("[porter error] Data is not a valid DataFrame.")

        return True

    def validate_column_metadata(self, column_metadata: dict):
        """
        Validate the format and content of column metadata.

        Parameters:
        - column_metadata (dict): Dictionary representing metadata for each column.

        Returns:
        - bool: True if the column metadata is valid, False otherwise.
        """
        # check that column_metadata is a dictionary
        if not isinstance(column_metadata, dict):
            raise ValueError("[porter error] Column metadata is not a dictionary.")

        # check that keys are strings
        if not all(isinstance(key, str) for key in column_metadata.keys()):
            raise ValueError("[porter error] Column metadata keys are not strings.")

        # check that values are tuples
        if not all(isinstance(val, tuple) for val in column_metadata.values()):
            raise ValueError("[porter error] Column metadata values are not tuples.")

        # check that tuples have length 2
        if not all(len(val) == 2 for val in column_metadata.values()):
            raise ValueError("[porter error] Column metadata tuples do not have length 2.")

        # check that first element of tuple is a string
        if not all(isinstance(val[0], str) for val in column_metadata.values()):
            raise ValueError("[porter error] Column metadata tuples first elements are not strings.")

        # check that second element of tuple is a type
        if not all(isinstance(val[1], type) for val in column_metadata.values()):
            raise ValueError("[porter error] Column metadata tuples second elements are not types.")

        return True

    def validate_data_keys_vs_column_metadata_keys(
        self, data_keys: dict, column_metadata_keys: dict
    ):
        """
        Validate that the keys of data and column metadata match.

        Parameters:
        - data_keys (dict): Keys of the data dictionary.
        - column_metadata_keys (dict): Keys of the column metadata dictionary.

        Returns:
        - bool: True if keys match, False otherwise.
        """
        # check that both dictionaries have the same keys
        if not set(data_keys) == set(column_metadata_keys):
            raise ValueError("[porter error] Data keys and column metadata keys do not match.")

        return True

    def set_column_metadata(self, column_metadata: dict):
        """
        Set column metadata.

        Parameters:
        - column_metadata (dict): Dictionary representing metadata for each column.
        """
        assert self.validate_column_metadata(column_metadata)
        self.column_metadata = column_metadata

    def get_column_metadata(self):
        """
        Get the stored column metadata.

        Returns:
        - dict: Dictionary representing metadata for each column.
        """
        return self.column_metadata

    def import_HDF5(self, filename: str, group: str = "default", dropna=False):
        """
        Import table data from an HDF5 file.

        Parameters:
        - filename (str): Path to the HDF5 file.
        - group (str): Name of the group within the HDF5 file.
        """
        # check that group is a string
        if not isinstance(group, str):
            print("[porter error] Group is not a string.")
            return False

        # check that the file exists
        try:
            with h5py.File(filename, "r") as f:
                pass
        except FileNotFoundError:
            print(f"[porter error] File {filename} not found.")
            return False

        with h5py.File(filename, "r") as f:
            # check that the group exists
            if group not in f:
                print(f"[porter error] Group {group} not found.")
                return False

            eos_group = f[group]

            data = {}
            for col in eos_group.keys():
                data[col] = eos_group[col][:]

            data = pd.DataFrame(data)

            column_metadata = {}
            for col in eos_group.keys():
                column_metadata[col] = (
                    eos_group[col].attrs["unit"],
                    yaml_type_to_numpy_dtype(eos_group[col].attrs["type"]),
                )

            # Update the object attributes with the new data and column metadata (validation happens in set_data)
            self.set_data(data.to_dict(orient="list"), column_metadata, dropna=dropna)

    def export_HDF5(self, filename: str, group: str = "default"):
        """
        Export table data to an HDF5 file.

        Parameters:
        - filename (str): Path to the HDF5 file.
        - group (str): Name of the group within the HDF5 file.
        """
        # check that group is a string
        if not isinstance(group, str):
            print("[porter error] Group is not a string.")
            return False

        with h5py.File(filename, "w") as f:
            eos_group = f.create_group(group)

            for col, (unit, dtype) in self.column_metadata.items():
                eos_group.create_dataset(col, data=self.data[col].values, dtype=dtype)
                eos_group[col].attrs["unit"] = unit
                eos_group[col].attrs["type"] = numpy_dtype_to_yaml_type(dtype)

    def import_CSV(
        self,
        filename: str,
        header: list[str],
        units: list[str],
        types: list[type],
        delimiter=",",
        dropna=False,
    ):
        """
        Import table data from a CSV file.

        Parameters:
        - filename (str): Path to the CSV file.
        - header (list): List of column names.
        - units (list): List of units for each column.
        - types (list): List of types for each column.
        - delimiter (str): Delimiter for CSV files (default is ",").
        - dropna (bool): Flag to drop NaN values from the data (default is False).
        """
        # Read CSV file
        data = pd.read_csv(
            filename,
            names=header,
            delimiter=delimiter,
            dtype={col: dtype for col, dtype in zip(header, types)},
            engine="python",
        )

        # Create updated column metadata dictionary col: (unit, dtype)
        updated_column_metadata = {
            col: (unit, dtype) for col, unit, dtype in zip(header, units, types)
        }

        # Update the object attributes with the new data and column metadata (validation happens in set_data)
        self.set_data(
            data.to_dict(orient="list"), updated_column_metadata, dropna=dropna
        )

    def export_metadata_to_yaml(self, filename: str):
        """
        Export column metadata to a YAML file.

        Parameters:
        - filename (str): Path to the YAML file.
        """

        # open empty yaml file
        with open(filename, "w") as yaml_file:
            pass

        # loop over the column metadata and append to yaml file using the order of the data headers
        for col in self.data.columns:
            unit, dtype = self.column_metadata[col]
            metadata_dict = {}

            metadata_dict[col] = {
                "description": col,
                "type": numpy_dtype_to_yaml_type(dtype),
                "x-unit": unit,
            }

            # append to yaml file
            with open(filename, "a") as yaml_file:
                yaml.dump(metadata_dict, yaml_file, default_flow_style=False)

    def export_CSV(self, filename: str, delimiter=",", float_format="%.12e"):
        """
        Export table data to a CSV file.

        Parameters:
        - filename (str): Path to the CSV file.
        - delimiter (str): Delimiter for CSV files (default is ",").
        - float_format (str): Format for floating point numbers (default is "%.12e").
        """
        self.data.to_csv(
            filename,
            index=False,
            header=False,
            sep=delimiter,
            float_format=float_format,
        )

    def display_data(self):
        """
        Display the stored data.
        """
        print("Data:")
        print(self.data)

    def display_column_metadata(self):
        """
        Display the stored column metadata.
        """
        print("Column metadata:")
        # display with the order of the data headers
        for col in self.data.columns:
            unit, dtype = self.column_metadata[col]
            print(f"Column: {col}, Unit: {unit}, Data Type: {dtype.__name__}")

    def display(self):
        """
        Display the stored data and metadata.
        """
        self.display_data()
        self.display_column_metadata()

    def reorder_columns(self, order: list[str]):
        """
        Reorder the columns of the data.

        Parameters:
        - order (list): List of column names in the desired order.
        """
        # check that all the columns in order are in the data
        if not all(col in self.data.columns for col in order):
            print("[porter error] Not all columns in order are in the data.")
            return False

        self.data = self.data[order]

    def slice_column_from_data(self, variables_to_keep: list[str]):
        """
        Slice the data to keep only the variables in variables_to_keep.

        Parameters:
        - variables_to_keep (list): List of variables to keep.
        """

        # Keep all the strings inside variables_to_keep that are also inside header
        variables_to_keep = [x for x in variables_to_keep if x in self.data.columns]
        # print the variables that are in variables_to_keep but not in header
        [
            print(f"[porter warning] Variable {x} not found in the header")
            for x in variables_to_keep
            if x not in self.data.columns
        ]

        self.data = self.data[variables_to_keep]
        self.column_metadata = {
            k: v for k, v in self.column_metadata.items() if k in variables_to_keep
        }

    def import_table(
        self,
        filename,
        extension="CSV",
        header=None,
        units=None,
        types=None,
        delimiter=",",
        variables_to_keep=None,
        dropna=False,
        verbose=False,
        group="default",
        order=None,
        filename_metadata=None,
        filename_schema=None,
        schema=None,
    ):
        """
        import_table data from a file.

        Parameters:
        - filename (str): Path to the file.
        - extension (str): File extension (default is "CSV").
        - header (list): List of column names for CSV (default is None).
        - units (list): List of units for each column for CSV (default is None).
        - types (list): List of types for each column for CSV (default is None).
        - delimiter (str): Delimiter for CSV files (default is ",").
        - filename_metadata (str): Path to the file with metadata for CSV (default is None).
        - group (str): Name of the group within the HDF5 file (default is "/").
        - variables_to_keep (list): List of variables to keep (default is None, keep all).
        - dropna (bool): Flag to drop NaN values from the data (default is False).
        - verbose (bool): Flag to display the data, column metadata, and file metadata (default is False).
        - filename_schema (str): Path to the OpenAPI schema file (default is None).
        - schema (str): Name of the schema within the OpenAPI file (default is None).
        """
        extension = extension.upper()

        if extension == "CSV":
            # handle specific CSV cases

            if header and units and types:

                if filename_metadata or filename_schema or schema:
                    print(
                        "[porter warning] Header, units, and types are provided, ignoring filename_metadata, and/or (filename_schema and schema)."
                    )

                if filename_metadata:
                    filename_metadata = None

                if filename_schema and schema:
                    filename_schema = None
                    schema = None

            elif filename_schema and not schema:
                print(
                    "[porter warning] Schema must be provided if filename_schema is provided, using filename_metadata instead."
                )
                filename_schema = None
                header, units, types = read_yaml_metadata(filename_metadata)
            elif filename_schema and schema:
                # If filename_schema and schema are provided, use their information
                header, units, types = read_openapi_schema(filename_schema, schema)
            elif filename_metadata:
                # If filename_metadata is provided, use its information
                header, units, types = read_yaml_metadata(filename_metadata)
            else:
                raise ValueError(
                    "[porter error] Header, units, and types must be provided for CSV files."
                )
                
            if header is None or units is None or types is None:
                raise ValueError(
                    "[porter error] Header, units, and types must be provided for CSV files."
                )

            if len(header) != len(units) and len(header) != len(types):
                raise ValueError(
                    "[porter error] Header, units, and types must have the same length."
                )

            self.import_CSV(
                filename, header, units, types, delimiter=delimiter, dropna=dropna
            )

        elif extension == "HDF5":
            # handle specific HDF5 cases

            if group is None:
                raise ValueError("[porter error] Group must be provided for HDF5 files.")

            self.import_HDF5(filename, group=group, dropna=dropna)

        else:
            raise ValueError(f"[porter error] Extension {extension} not supported.")

        if dropna:
            # Drop NaN values from the stored data
            self.data = self.data.dropna()

        if variables_to_keep:
            self.slice_column_from_data(variables_to_keep)

        if order:
            self.reorder_columns(order)

        if verbose:
            self.display()

    def export_table_updating_data(
        self,
        filename,
        extension="CSV",
        delimiter=",",
        float_format="%.12e",
        verbose=False,
        variables_to_keep=None,
        order=None,
        dropna=False,
        group="default",
        filename_metadata=None,
        filename_schema=None,
        schema=None,
    ):
        """
        Export table data to a file modifying the stored object.

        Parameters:
        - filename (str): Path to the file.
        - extension (str): File extension (default is "CSV").
        - delimiter (str): Delimiter for CSV files (default is ",").
        - float_format (str): Format for floating point numbers (default is "%.12e").
        - verbose (bool): Flag to display the data, column metadata, and file metadata (default is False).
        - variables_to_keep (list): List of variables to keep (default is None, keep all).
        - order (list): List of column names in the desired order.
        - dropna (bool): Flag to drop NaN values from the data (default is False).
        - group (str): Name of the group within the HDF5 file (default is "default").
        - filename_metadata (str): Path to the YAML metadata file (default is None).
        - filename_schema (str): Path to the OpenAPI schema file (default is None).
        - schema (str): Name of the schema within the OpenAPI file (default is None).
        """
        extension = extension.upper()

        if filename_schema and not schema:
            print(
                "[porter warning] Schema must be provided if filename_schema is provided, ignoring filename_schema."
            )
            filename_schema = None
        elif filename_schema and schema:
            # If filename_schema and schema are provided, use their information
            header_schema, units_schema, types_schema = read_openapi_schema(
                filename_schema, schema
            )

            # check that header_schema is contained in the header of the data
            if not all(col in self.data.columns for col in header_schema):
                raise ValueError(
                    "[porter error] Not all columns in header_schema are in the data."
                )

            # check that units_schema are equal to the unit from metadata
            if not all(
                self.column_metadata[col][0] == unit
                for col, unit in zip(header_schema, units_schema)
            ):
                raise ValueError(
                    "[porter error] Not all units in units_schema are equal to the units from metadata."
                )

            # check that types_schema are equal types from metadata
            if not all(
                self.column_metadata[col][1] == dtype
                for col, dtype in zip(header_schema, types_schema)
            ):
                raise ValueError(
                    "[porter error] Not all types in types_schema are equal to the types from metadata."
                )

            # update the metadata

            # slice the data to keep only the variables in the schema
            self.slice_column_from_data(header_schema)

            # reorder the columns
            self.reorder_columns(header_schema)

        if dropna:
            # Drop NaN values
            self.data = self.data.dropna()

        if variables_to_keep:
            self.slice_column_from_data(variables_to_keep)

        if order:
            self.reorder_columns(order)

        if verbose:
            self.display()

        if extension == "CSV":
            self.export_CSV(filename, delimiter=delimiter, float_format=float_format)
        elif extension == "HDF5":
            if group is None:
                raise ValueError("[porter error] Group must be provided for HDF5 files.")

            self.export_HDF5(filename, group=group)
        else:
            raise ValueError(f"[porter error] Extension {extension} not supported.")

        # Write metadata to a separate YAML file
        if filename_metadata:
            self.export_metadata_to_yaml(filename_metadata)

    def export_table(
        self,
        filename,
        extension="CSV",
        delimiter=",",
        float_format="%.12e",
        variables_to_keep=None,
        order=None,
        dropna=False,
        group="default",
        filename_metadata=None,
        filename_schema=None,
        schema=None,
        verbose=False,
    ):
        """
        Export table data to a file without modifying the stored object.

        Parameters:
        - filename (str): Path to the file.
        - extension (str): File extension (default is "CSV").
        - delimiter (str): Delimiter for CSV files (default is ",").
        - float_format (str): Format for floating point numbers (default is "%.12e").
        - variables_to_keep (list): List of variables to keep (default is None, keep all).
        - order (list): List of column names in the desired order.
        - dropna (bool): Flag to drop NaN values from the data (default is False).
        - group (str): Name of the group within the HDF5 file (default is "default").
        - filename_metadata (str): Path to the YAML metadata file (default is None).
        - verbose (bool): Flag to display the data, column metadata, and file metadata (default is False).
        """
        # Create a new instance of the Porter class to avoid modifying the existing object
        temp_porter = Porter()

        # Copy the necessary data and metadata to the new instance
        temp_porter.data = self.data.copy()
        temp_porter.column_metadata = self.column_metadata.copy()

        # Perform export operations on the new instance without modifying the original object
        temp_porter.export_table_updating_data(
            filename,
            extension=extension,
            delimiter=delimiter,
            float_format=float_format,
            variables_to_keep=variables_to_keep,
            order=order,
            dropna=dropna,
            group=group,
            filename_metadata=filename_metadata,
            verbose=verbose,
            filename_schema=filename_schema,
            schema=schema,
        )
