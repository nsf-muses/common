#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
TestPorter.py: A script with the unit tests for the Porter class for handling scientific data and metadata.
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
"""

import unittest
import pandas as pd
from muses_porter import Porter

class TestPorterValidation(unittest.TestCase):
    def setUp(self):
        # Initialize a Porter instance for testing
        self.porter = Porter()

    def test_validate_data_valid(self):
        # Test case where data is valid
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        result = self.porter.validate_data(data)
        self.assertTrue(result)

    def test_validate_data_invalid_type(self):
        # Test case where data is not a dictionary
        data = 'invalid_data'
        with self.assertRaises(ValueError):
            __ = self.porter.validate_data(data)
        

    def test_validate_data_invalid_keys(self):
        # Test case where data keys are not strings
        data = {1: [1, 2, 3], 'col2': ['a', 'b', 'c']}
        with self.assertRaises(ValueError):
            __ = self.porter.validate_data(data)

    def test_validate_data_invalid_values(self):
        # Test case where data values are not lists
        data = {'col1': 'invalid_data', 'col2': ['a', 'b', 'c']}
        with self.assertRaises(ValueError):
            __ = self.porter.validate_data(data)

    def test_validate_column_metadata_valid(self):
        # Test case where column metadata is valid
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', float)}
        result = self.porter.validate_column_metadata(column_metadata)
        self.assertTrue(result)

    def test_validate_column_metadata_invalid_type(self):
        # Test case where column metadata is not a dictionary
        column_metadata = 'invalid_metadata'
        with self.assertRaises(ValueError):
            __ = self.porter.validate_column_metadata(column_metadata)
        

    def test_validate_column_metadata_invalid_keys(self):
        # Test case where column metadata keys are not strings
        column_metadata = {1: ('unit1', int), 'col2': ('unit2', float)}
        with self.assertRaises(ValueError):
            __ = self.porter.validate_column_metadata(column_metadata)
        

    def test_validate_column_metadata_invalid_values(self):
        # Test case where column metadata values are not tuples
        column_metadata = {'col1': 'invalid_metadata', 'col2': ('unit2', float)}
        with self.assertRaises(ValueError):
            __ = self.porter.validate_column_metadata(column_metadata)
        

    def test_validate_keys_match_valid(self):
        # Test case where keys match
        data_keys = {'col1', 'col2'}
        column_metadata_keys = {'col1', 'col2'}
        result = self.porter.validate_data_keys_vs_column_metadata_keys(data_keys, column_metadata_keys)
        self.assertTrue(result)

    def test_validate_keys_match_invalid(self):
        # Test case where keys do not match
        data_keys = {'col1', 'col2'}
        column_metadata_keys = {'col1', 'col3'}
        with self.assertRaises(ValueError):
            __ = self.porter.validate_data_keys_vs_column_metadata_keys(data_keys, column_metadata_keys)
        

    def test_set_data_valid(self):
        # Test case where data and column metadata are valid
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}
        self.porter.set_data(data, column_metadata)

        expected_data_str = pd.DataFrame(data).to_string(index=False)
        actual_data_str = self.porter.get_data().to_string(index=False)

        self.assertEqual(actual_data_str, expected_data_str)
        self.assertEqual(self.porter.get_column_metadata(), column_metadata)

    def test_set_data_invalid_data(self):
        # Test case where data is invalid
        data = 'invalid_data'
        column_metadata = {'col1': ('unit1', int)}
        with self.assertRaises(ValueError):
            self.porter.set_data(data, column_metadata)

    def test_set_data_invalid_column_metadata(self):
        # Test case where column metadata is invalid
        data = {'col1': [1, 2, 3]}
        column_metadata = 'invalid_metadata'
        with self.assertRaises(ValueError):
            self.porter.set_data(data, column_metadata)

    def test_set_data_keys_mismatch(self):
        # Test case where keys in data and column metadata do not match
        data = {'col1': [1, 2, 3]}
        column_metadata = {'col2': ('unit1', int)}
        with self.assertRaises(ValueError):
            self.porter.set_data(data, column_metadata)

    def test_set_data_valid_dropna(self):
        # Test case where data and column metadata are valid
        data = {'col1': [1, 2, None], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}
        self.porter.set_data(data, column_metadata, dropna=True)

        expected_data_str = pd.DataFrame(data).dropna().to_string(index=False)
        actual_data_str = self.porter.get_data().to_string(index=False)

        self.assertEqual(actual_data_str, expected_data_str)
        self.assertEqual(self.porter.get_column_metadata(), column_metadata)

    def test_export_CSV(self):
        # Test case for importing and exporting CSV file
        filename = "/tmp/test_data.csv"
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}

        # Export data to CSV
        self.porter.set_data(data, column_metadata)

        self.porter.export_CSV(filename)
        
        # Read exported CSV file
        exported_data = pd.read_csv(filename, names=['col1', 'col2'], header=None, index_col=False, dtype={'col1': int, 'col2': str})

        # Compare data
        self.assertTrue(exported_data.equals(self.porter.get_data()))
        self.assertEqual(self.porter.get_column_metadata(), column_metadata) # metadata is destroyed in export csv

    def test_export_CSV_string_content(self):
        # Test case for importing and exporting CSV file
        filename = "/tmp/test_data.csv"
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}

        # Export data to CSV
        self.porter.set_data(data, column_metadata)

        self.porter.export_CSV(filename)
        
        # Read exported CSV file
        exported_data = pd.read_csv(filename, names=['col1', 'col2'], header=None, index_col=False, dtype={'col1': int, 'col2': str})

        # Compare data
        self.assertTrue(exported_data.equals(self.porter.get_data()))
        self.assertEqual(self.porter.get_column_metadata(), column_metadata)

    def test_import_CSV(self):
        # Test case for importing and exporting CSV file
        filename = "/tmp/test_data.csv"
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}

        # Export data to CSV
        self.porter.set_data(data, column_metadata)
        self.porter.export_CSV(filename)

        # Import data from CSV
        self.porter.import_CSV(filename, header=['col1', 'col2'], units=['unit1', 'unit2'], types=[int, str])

        # Compare data
        self.assertTrue(self.porter.get_data().equals(pd.DataFrame(data)))
        self.assertEqual(self.porter.get_column_metadata(), column_metadata) # metadata is destroyed in export csv

    def test_export_HDF5(self):
        # Test case for importing and exporting HDF5 file
        filename = "/tmp/test_data.h5"
        data = {'col1': [1, 2, 3], 'col2': [3, 4, 5]}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', int)}

        # Export data to HDF5
        self.porter.set_data(data, column_metadata)
        self.porter.export_HDF5(filename, group='test')

        # Reset the Porter instance to clear data and metadata
        self.porter.reset()

        # Import data from HDF5
        self.porter.import_HDF5(filename, group='test')

        # Compare data
        self.assertTrue(self.porter.get_data().equals(pd.DataFrame(data)))
        self.assertEqual(self.porter.get_column_metadata(), column_metadata)


    def test_export_HDF5_string_content(self):
        # Test case for importing and exporting HDF5 file
        filename = "/tmp/test_data.h5"
        data = {'col1': [1, 2, 3], 'col2': ['a', 'b', 'c']}
        column_metadata = {'col1': ('unit1', int), 'col2': ('unit2', str)}

        # Export data to HDF5
        self.porter.set_data(data, column_metadata)
        self.porter.export_HDF5(filename, group='test')

        print("1", self.porter.Display())

        # Reset the Porter instance to clear data and metadata
        self.porter.reset()

        # Import data from HDF5
        self.porter.import_HDF5(filename, group='test')

        print("2", self.porter.Display())

        # Compare data
        self.assertTrue(self.porter.get_data().equals(pd.DataFrame(data)))
        self.assertEqual(self.porter.get_column_metadata(), column_metadata)

if __name__ == '__main__':
    unittest.main()
