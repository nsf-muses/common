# MUSES common base image

This repo defines the common base image for many of the modules used by the MUSES calculation engine.

Tagged images are published to the container registry `registry.gitlab.com/nsf-muses/common/muses-common`.

The git tags of this repo mark the commits used to build the Docker images tagged with the same version labels, where version numbers adhere to semantic versioning conventions.

**Note**: Reproducibility of image builds are not completely guaranteed because some dependencies are not immutable (e.g. system packages installed via `apt`).
