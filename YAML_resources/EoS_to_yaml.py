import yaml
import numpy as np

def main():
  eos_table=np.loadtxt('CMF_MeV.dat')

  # Rebuild the data table as a list of dictionaries
  data_table=[]
  for numpy_row in eos_table:
    # yaml output of numpy arrays is a mess,
    # so convert the row from a numpy array to a regular list
    row=numpy_row.tolist()  
    data_dict={
     'baryon_density': row[0],
     'temperature': row[1],
     'charge_fraction': row[2],
     'entropy_density': row[3],
     'mu_p': row[4],
     'mu_n': row[5], 
     'energy_density': row[6], 
     'pressure': row[7],
     'proton_density': row[8],
     'neutron_density': row[9],
     'M_p': row[10],
     'M_n': row[11],
     'U_p': row[12],
     'U_n': row[13]
    }
    data_table.append(data_dict)

  # data_table is now a list of dictionaries, 
  # one for each row of the original table

  field_units_dict={
   'baryon_density': 'MeV^4',
   'temperature':'MeV',
   'charge_fraction': '',
   'entropy_density': 'MeV^3',
   'mu_p': 'MeV',
   'mu_n': 'MeV',
   'energy_density': 'MeV^4',
   'pressure': 'MeV^4',
   'proton_density': 'MeV^3',
   'neutron_density': 'MeV^3',
   'M_p': 'MeV',
   'M_n': 'MeV',
   'U_p': 'MeV',
   'U_n': 'MeV'
   }

  descriptions_dict={
   'M_p': 'effective mass of proton',
   'M_n': 'effective mass of neutron',
   'U_p': 'energy shift for neutron, added to kinetic energy',
   'U_n': 'energy shift for proton, added to kinetic energy'
  }

  # Assmeble everything into the data structure that we will export 
  # in YAML format
  eos_file_dict={
  'apiVersion': '1.0.0',
  'kind': 'Equation_Of_State',
  'metadata':
   {'name': 'CMF version X.Y',
    'description': 'CMF equation of state for hadronic matter',
    'namespace':''
   },
  'spec': {
   'descriptions': descriptions_dict,
   'units': field_units_dict,
   'values': data_table
   }
  }

  # Dump it to a YAML file
  outfile=open('CMF_MeV.yml','w')
  yaml.dump(eos_file_dict,outfile)

if __name__=="__main__":
  main()

