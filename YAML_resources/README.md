## Resources for making and reading YAML files

Code examples for making and reading YAML files


- EoS_to_yaml.py <br/>
A python script that reads in the CMF EOS as a table of numbers from CMF_EoS.dat, and outputs them as a YAML file CMF_MeV.yml


