# yaml-cpp wrapper

Wrapper for easily importing/exporting data tables in yaml format.

**Disclaimer:** Preliminary version. Implementation of EoS format proposed by Mark Alford is still missing!

## 1. Formats

So far, two kinds of data structure are supported:
- `yaml_list`: List of values (`std::vector <double>`) + units (`std::string`) + functionalities.
- `yaml_table`: `std::vector` of `yaml_list` + functionalities.



## 2. Examples

Both formats can be directly imported/exported from a yaml file, for example ( see `./src/test.cpp` and `./sample.yaml`).

### 2.1. yaml-list

        /*========== yaml-list ===========*/
        
        /******** Import ******************/
        // import subnode "mass" as a yaml_list object!
        yaml_list mass_list = mass.as<yaml_list>();
        /******** Export ******************/
        // directly assign yaml_list objects to nodes!
        newconfig["mass"] = mass_list;
        /****** Export + Import ***********/
        // import and export radius from sample file to new configuration
        newconfig["radius"] = spec["radius"].as<yaml_list>();

### 2.2. yaml-table


        /*========== yaml-table ===========*/
        
        /******** Import ******************/
        // import whole "spec" node as a yaml_table object!
        yaml_table spectable = spec.as<yaml_table>();
        
        // reorder columns! radius before mass.
        std::vector<size_t> order = {1,0};
        spectable.reorder_col(order);
        
        /******** Convert ******************/
        // print table to .dat file
        std::ofstream fdat("table.dat");
        spectable.print(fdat, ",");
        
        /******** Export ******************/
        // and assign it no node "table" in newconfig node
        newconfig["table"] = spectable;

## 3. Building

### 3.1. Make

The library can be compiled from the root directory:
```
$ make
``` 

### 3.2. Non-root install

Running
```
$ make install
```
will by default install the library in `/home/$user/.local/`. 

### 3.3. System-wide installation

For systemwide installation run 
```
$ sudo PREFIX=/usr/local/ make install
```
which requires root priviledges.
