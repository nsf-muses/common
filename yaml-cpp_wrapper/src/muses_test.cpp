#include <stdlib.h>
#include "yaml-cpp/yaml.h"
#include "muses_yaml.h"
#include <iostream>
#include <fstream>


int main()
{
	/*========== declarations ===========*/
	// creates new node
    YAML::Node newnode; 

	/******** Convert ******************/
	// print table to .dat file
	// std::ofstream fdat("table.dat");
	// spectable.print(fdat, ",");

     muses_metadata metadata("test-table","alpha:0.0.0","table1","mass-radius relation");
     std::vector<std::string> keys = {"M", "R"};
     std::vector<std::string> units = {"Msun","km"};
     std::vector<std::string> descripts = {"mass of neutron star","radius of neutron star"};
     std::vector<size_t> shape = {2,4};
     muses_header structure(keys,units,descripts,shape);
     muses_table table(metadata,structure);
     table.set_entry({0,0},{2.,10.});
     table.set_entry({1,0},{1.4,12.});
     table.set_entry({0,1},{1.,15.});
     table.set_entry({1,1},{0.1,100.});
     table.set_entry({0,2},{2.,10.});
     table.set_entry({1,2},{2.,10.});
     table.set_entry({0,3},{2.,10.});
     table.set_entry({1,3},{2.,10.});

    //  muses_header structure(keys,units,descripts,8);
    //  muses_table table(metadata,structure);
    //  table.push_back({0,0},{2.,10.});
    //  table.push_back({1,0},{1.4,12.});
    //  table.push_back({0,1},{1.,15.});
    //  table.push_back({1,1},{0.1,100.});
    //  table.push_back({0,2},{1e-4,1e26});
    //  table.push_back({1,2},{2.,10.});
    //  table.push_back({0,3},{2.,10.});
    //  table.push_back({1,3},{2.,10.});

    //  muses_header structure(keys,units,descripts,shape);
    //  muses_table table(metadata,structure);
    // //  muses_header structure(keys,units,descripts,8);
    // //  muses_table table(metadata,structure);
    //  #pragma omp parallel for shared(table)
    //  for(size_t i = 0; i < 8; i++)
    //  {
    //     table.set_entry({i%2,i/2},{2.,10.+(double) i});
    //     // table.push_back({i%2,i/2},{2.,10.+(double) i});
    //  }

    // std::cout << table.shape()[0] << "  " << table.shape()[1] << std::endl;
    table.order_by_index({1,0});
    // table.order_by_index({0,1});
    //  table.push_back()

	/******** Export ******************/
	// and assign it no node "table" in newconfig node
	newnode["table"] = table;

	/*====== save to output ========*/
	// directly stream newconfig node
	std::ofstream fout("muses-out.yaml");
	fout  << std::scientific << newnode <<"\n";
	std::ofstream foutcsv("muses-out.csv");
	table.print(foutcsv," ", true);
    fout.close();

	/*====== reimport and print ========*/
    muses_table same = newnode["table"].as<muses_table>();
    YAML::Node samenode;
    samenode["table"] = same;
    std::cout << std::scientific << samenode << std::endl;
    
    /*====== export to mixed YAML-DSV format*/
    YAML::Node meta;
    same.save(meta,"test.csv",", ");
    
	std::ofstream mixedout("muses-mixed-out.yaml");
    mixedout << meta;
    mixedout.close();

    return 0;
}
