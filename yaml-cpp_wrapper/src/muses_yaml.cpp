#include "muses_yaml.h"
#include <fstream>
#include <limits>

// struct for storing table metadata
// constructor
muses_metadata::muses_metadata(std::string type, std::string version, std::string title, std::string description) : type(type), version(version), title(title), description(description) {}
// print metadata
void muses_metadata::print(std::ostream &out) const
{
    out << "## type: " <<  type << "\n## version: " << version <<  "\n## description: " << description << std::endl;
}
// get YAML metadata
YAML::Node muses_metadata::get_yaml() const
{
    YAML::Node yout;
    yout["type"] = type;
    yout["title"] = title;
    yout["version"] = version;
    yout["description"] = description;
    return yout;
}

// struct with table dimensions, columns, keys and units
// constructor
muses_header::muses_header(std::vector <std::string> keys, std::vector<size_t> shape): keys(keys), shape(shape) 
{
    // get number of columns and units
    ncols = keys.size();
    for(size_t col = 0; col < ncols; col++)
    {
        key_to_col[keys[col]] = col;
    }
    std::vector<std::string> un(ncols,"");
    units = un;
    // and description of columns
    std::vector<std::string> desc(ncols,"");
    descriptions = desc;

    // get dimension from shape
    index_dimension = shape.size();
    // get overall size from shape  
    size = size_from_shape(shape);
}
muses_header::muses_header(std::vector <std::string> keys, std::vector <std::string> unitsx, std::vector<size_t> shape): keys(keys), units(unitsx), shape(shape) 
{
    // get number of columns
    ncols = keys.size();
    for(size_t col = 0; col < ncols; col++)
    {
        key_to_col[keys[col]] = col;
    }
    // and description of columns
    std::vector<std::string> desc(ncols,"");
    descriptions = desc;

    // get dimension from shape
    index_dimension = shape.size();
    // get overall size from shape
    size = size_from_shape(shape);  
    // missing units
    for(size_t i = units.size(); i < keys.size(); i++)
    {
        units.push_back("");
    }
}
muses_header::muses_header(std::vector <std::string> keys, std::vector <std::string> unitsx, std::vector <std::string> descriptionsx, std::vector<size_t> shape): keys(keys), units(unitsx), descriptions(descriptionsx), shape(shape) 
{
    // get number of columns
    ncols = keys.size();
    for(size_t col = 0; col < ncols; col++)
    {
        key_to_col[keys[col]] = col;
    }
    // get dimension from shape
    index_dimension = shape.size();
    // get overall size from shape  
    size = size_from_shape(shape);
    // missing units
    for(size_t i = units.size(); i < keys.size(); i++)
    {
        units.push_back("");
    }
    // missing descriptions
    for(size_t i = descriptions.size(); i < keys.size(); i++)
    {
        descriptions.push_back("");
    }
}
// print header
void muses_header::print(std::ostream &out, std::string sep) const
{
    out << "# ";
    for(size_t col = 0; col < ncols; col++)
    {
        out << keys[col];
        if(units[col] != "")
        {
            out << " [" << units[col] << "]";
        }
        if(col<ncols-1)
        {
            out << sep;
        }
    }
    out << std::endl;
}

// get header in yaml
YAML::Node muses_header::get_yaml() const
{
    // nodes for output
    YAML::Node yout;
    // subnodes
    YAML::Node ydescriptions;
    YAML::Node yunits;
    YAML::Node colmap;
    // loop over columns
    for(size_t col = 0; col < ncols; col++)
    {
        colmap[keys[col]] = col;
        ydescriptions[keys[col]] = descriptions[col];
        if(units[col] != "")
        {
            yunits[keys[col]] = units[col];
        }else{
            // no units
            yunits[keys[col]] = "1";
        }
    }
    // save to output
    yout["column_to_index"] = colmap;
    yout["units"] = yunits;
    yout["descriptions"] = ydescriptions;
    return yout;
}
// YAML::Node muses_header::get_yaml() const
// {
//     // nodes for output
//     YAML::Node yout;
//     // subnodes
//     YAML::Node ylabels;
//     YAML::Node yunits;
//     YAML::Node ydescriptions;
//     // loop over columns
//     for(size_t col = 0; col < ncols; col++)
//     {
//         ylabels.push_back(keys[col]);
//         ydescriptions[keys[col]] = descriptions[col];
//         if(units[col] != "")
//         {
//             yunits[keys[col]] = units[col];
//         }else{
//             // no units
//             yunits[keys[col]] = "1";
//         }
//     }
//     // save to output
//     yout["labels"] = ylabels;
//     yout["units"] = yunits;
//     yout["descriptions"] = ydescriptions;
//     return yout;
// }

// validate if a key is there
int muses_header::validate_key(std::string key)
{
    if(std::find(keys.begin(),keys.end(),key) != keys.end())
    {
        return 0;
    }else{
        std::cerr << "muses_header: Error! Key not found!" << std::endl;
        return 1;
    }
}

// Class for table in MUSES format
// default constructor necessary for yaml-cpp integration
muses_table::muses_table() : header({},{})
{
    // std::vector <std::string> keys;
    // std::vector<size_t> shape;
    metadata = muses_metadata();
    // header = muses_header(keys,shape);
}
// copy constructor
// muses_table::muses_table(muses_table& other)
// {
//     metadata = other.metadata;
//     header = other.header;
//     entries = other.entries;
//     order = other.order;
//     indices = other.indices;
//     indices_to_pos = other.indices_to_pos;
// }



// constructor not filling table with anything
muses_table::muses_table(muses_metadata metadata, muses_header header) : metadata(metadata), header(header) 
{
    // required size
    size_t size = header.size;
    // if so, allocate and prepare order already
    ordered.resize(size);
    entries.resize(size);
    indices.resize(size);

    // not filled
    std::vector<bool> lfilled(size,false);
    filled = lfilled;


    // get dimensions and shape
    size_t dim = header.index_dimension;
    auto shape = header.shape;
    // loop over all entries by position
    for(size_t pos = 0; pos < size; pos++)
    {
        // loop over all dimensions to get indices
        size_t divisor = 1;
        std::vector<size_t> index(shape.size(),0);
        for(size_t d = 0; d < dim; d++) 
        {
            index[d] = (pos/divisor)  % shape[d];
            divisor *= shape[d];
        }
        ordered[pos] = pos;
        indices[pos] = index;
        indices_to_pos[index] = pos;

        std::vector<double> default_entry(header.ncols,std::numeric_limits<double>::quiet_NaN());
        entries[pos] =  default_entry;
    }

}
// constructor filling table with a default value
muses_table::muses_table(muses_metadata metadata, muses_header header, double default_val) : metadata(metadata), header(header) 
{
    // required size
    size_t size = header.size;
    // if so, allocate and prepare order already
    ordered.resize(size);
    entries.resize(size);
    indices.resize(size);

    // all filled
    std::vector<bool> lfilled(size,true);   
    filled = lfilled;

    // get dimensions and shape
    size_t dim = header.index_dimension;
    auto shape = header.shape;
    // loop over all entries by position
    for(size_t pos = 0; pos < size; pos++)
    {
        // loop over all dimensions to get indices
        size_t divisor = 1;
        std::vector<size_t> index(shape.size(),0);
        for(size_t d = 0; d < dim; d++) 
        {
            index[d] = (pos/divisor)  % shape[d];
            divisor *= shape[d];
        }
        ordered[pos] = pos;
        indices[pos] = index;
        indices_to_pos[index] = pos;

        std::vector<double> default_entry(header.ncols,default_val);
        entries[pos] =  default_entry;
    }

}

void muses_table::push_back(std::vector <size_t> index, std::vector<double> entry) 
{
    // will not check for doubled indices for now!
    // just for the sake of performance

    // // look for entry with same indices
    // auto it = indices_to_pos.find(index);
    // if(it == indices_to_pos.end() )
    // {
    //     // no entry found
    // }
    entries.push_back(entry);
    indices.push_back(index);
    auto pos = ordered.size();
    indices_to_pos[index] = pos;
    ordered.push_back(pos);
    filled.push_back(true);
    // validate entry
    validate_entry(entry);
    // check if dimensions were determined
    if(header.index_dimension == 0)
    {
        // dimensions are determined now!
        header.index_dimension = index.size();
    }else{
        // check indices
        validate_index(index);
    }   
 
}
// insert
// void muses_table::insert(size_t pos, std::vector <size_t> index, std::vector<double> entry) 
// { 
//     // will not check for doubled indices for now!
//     // just for the sake of performance
//     entries.insert(entries.begin() + pos,entry);
//     indices.insert(indices.begin() + pos, index);
//     indices_to_pos[index] = pos;
//     // first add entry to order
//     order.push_back(pos);
//     // will have to shift order
//     std::rotate(order.begin() + pos, order.end() - 1, order.end() );
// }
// set entry by order
void muses_table::set_entry(size_t order, std::vector<double> entry) 
{
    // validate
    validate_entry(entry);
    // fill
    size_t pos = ordered[order];
    entries[pos] = entry;
    // filled
    if(!entry.empty())
    {
        filled[pos] = true;
    }
}
void muses_table::set_entry(size_t order, std::string key, double value)
{
    // validate
    header.validate_key(key);
    // fill  
    size_t pos = ordered[order];
    entries[pos][header.key_to_col[key]] = value;
    // filled
    filled[pos] = true;
}
void muses_table::set_entry(size_t order, size_t col, double value)
{
    // fill
    size_t pos = ordered[order];
    entries[pos][col] = value;
    // filled
    filled[pos] = true;
}
// set entry by index
void muses_table::set_entry(std::vector <size_t> index, std::vector<double> entry) 
{
    // validate
    validate_entry(entry);
    validate_index(index);
    // fill
    auto pos = indices_to_pos[index];
    entries[pos] = entry;
    // filled
    if(!entry.empty())
    {
        filled[pos] = true;
    }

}
void muses_table::set_entry(std::vector <size_t> index, std::string key, double value)
{
    // validate
    header.validate_key(key);
    validate_index(index);
    // fill
    auto pos = indices_to_pos[index];
    entries[pos][header.key_to_col[key]] = value;
    // filled
    filled[pos] = true;
}
void muses_table::set_entry(std::vector <size_t> index, size_t col, double value)
{
    // validate
    validate_index(index);
    // fill
    auto pos = indices_to_pos[index];
    entries[pos][col] = value;    
    // filled
    filled[pos] = true;
}
// reading
// get size
size_t muses_table::size()
{
    return ordered.size();
}
// get entry by order
std::vector<double> muses_table::get_entry(size_t order) 
{
    size_t pos = ordered[order];
    if(filled[pos])
    {
        return entries[pos];
    }else{
        std::vector<double> empty;
        return empty;
    }
}
double muses_table::get_entry(size_t order, std::string key)
{
    // pos = ordered[order]
    return entries[ordered[order]][header.key_to_col[key]];
}
double muses_table::get_entry(size_t order, size_t col)
{
    // pos = ordered[order]
    return entries[ordered[order]][col];
}
// get entry by index
std::vector<double> muses_table::get_entry(std::vector <size_t> index) 
{
    validate_index(index);
    size_t pos = indices_to_pos[index];
    if(filled[pos])
    {
        return entries[pos];
    }else{
        std::vector<double> empty;
        return empty;
    }
}
double muses_table::get_entry(std::vector <size_t> index, std::string key)
{
    validate_index(index);
    header.validate_key(key);
    return entries[indices_to_pos[index]][header.key_to_col[key]];
}
double muses_table::get_entry(std::vector <size_t> index, size_t col)
{
    validate_index(index);
    return entries[indices_to_pos[index]][col];
}
// // order by key
// void muses_table::order_by_key(std::string key, bool ascending)
// {
// }

// order by index, with priority to last one in increasing priority order. default: ascending order
void muses_table::order_by_index(std::vector<size_t> order_priority, bool ascending)
{
    // get dimensions and shape
    size_t dim = header.index_dimension;
    auto shape = header.shape;
    // loop over all possible entries
    // size_t total_len = size_from_shape(shape);
    // count actual order
    size_t ord = 0;
    if(not ascending)
    {
        ord = size()-1;
    }
    // scan in tentative order (one if table is all filled)
    size_t try_ord = 0;
    // loop over all possible indices
    do
    {
        // build index for this position
        std::vector<size_t> index(dim,0);
        // rounding up factor
        size_t divisor = 1;
        // loop over all dimensions
        // start from lowest priority
        for(size_t p = 0; p < dim; p++)
        {
            // which dimension is it? (priority from zero to dim-1, ascending order)
            size_t d = order_priority[p];
            // find index
            index[d] = (try_ord/divisor)  % shape[d];
            // rounding up factor
            divisor *= shape[d];            
        }
        // check if this index is listed
        auto it = std::find(indices.begin(), indices.end(), index);
        if(it != indices.end())
        {
            // it is listed, actual order found
            // find position
            size_t pos = indices_to_pos[index];
            // store order
            ordered[ord] = pos;
            // go to next in order
            if(ascending)
            {
                ord++;
            }else{
                ord--;
            }
        }
        // try next possibility
        try_ord++;
    }while(ord < size()); // untill all are found in order

}
std::vector<size_t> muses_table::shape()
{
    return header.shape;
}
// print
void muses_table::print(std::ostream &out, std::string sep, bool print_indices) const
{
    
    out.precision(25);
    out << std::scientific;
    // metadata
    metadata.print(out);
    // header
    // print indices first
    if(print_indices)
    {    
        out << "# ";
        // loop over indices
        for(size_t j = 0; j < header.index_dimension; j++)
        {
            out << "i" << j << sep;
        }
    }
    header.print(out,sep);
    // table
    // loop over rows
    for(size_t i = 0; i < ordered.size(); i++)
    {
        // convert order to position
        size_t pos = ordered[i];
        // check if filled
        if(!filled[pos]) 
        {
            continue;
        }
        // print indices first
        if(print_indices)
        {     
            // loop over indices
            for(size_t j = 0; j < header.index_dimension; j++)
            {
                out << indices[pos][j] << sep;
            }
        }
        // print values
        // loop over columns
        for(size_t j = 0; j < header.ncols; j++)
        {
            out << entries[pos][j];
            if(j<header.ncols-1) 
            {
                out << sep;
            }
        }
        out << "\n";
    }
}
// save table in text format and return yaml metadata
void muses_table::save(YAML::Node& yaml_out, std::string filename, std::string sep) const
{
    // Save table file
    std::ofstream outfile(filename,std::ios_base::out); 
    outfile  << std::scientific;
    outfile.precision(25);
    print(outfile,sep,false);
    outfile.close();

    // make table metadata
    YAML::Node yout = metadata.get_yaml();
    yout["column_metadata"] = header.get_yaml();
    yout["dsv_filename"] = filename;   
    // output YAML via reference
    yaml_out[metadata.title] = yout;

    return;

}

// assignment overload necessary for yaml-cpp integration
muses_table& muses_table::operator=(const muses_table& other)
{
    metadata = other.metadata;
    header = other.header;
    entries = other.entries;
    ordered = other.ordered;
    indices = other.indices;
    indices_to_pos = other.indices_to_pos;
    filled = other.filled;

    return *this;
}
// operator (order)
std::vector<double> muses_table::operator() (size_t pos) 
{
    return get_entry(pos);
}
// operator (index)
std::vector<double> muses_table::operator() (std::vector <size_t> index) 
{
    return get_entry(index);
}


// check if entry is valid
int muses_table::validate_entry(std::vector<double> entry)
{
    // check length
    if(entry.size() != header.ncols)
    {
        std::cerr << "muses_table: Error! Number of columns in entry does not match table!!!" << std::endl;
        return 1;
    }
    return 0;
}
// check if indices are valid
int muses_table::validate_index(std::vector<size_t> index)
{   
    // check length
    if(index.size() != header.index_dimension and  header.index_dimension !=0)
    {
        std::cerr << "muses_table: Error! Index dimensions do not match table!!!" << std::endl;
        return 1;
    }
    return 0;
}
// check if index exists
bool muses_table::index_exists(std::vector<size_t> index)
{
    // check if this index is listed
    auto it = std::find(indices.begin(), indices.end(), index);
    if(it == indices.end())
    {
        return false;
    }
    return true;
    
}
// check if there is overflow of table dimension
std::pair<size_t,size_t> muses_table::shape_overflow(std::vector<size_t> index)
{
    // check each index
    for(size_t d = 0; d < header.index_dimension; d++)
    {
        if(header.shape[d] <= index[d])
        {
            std::cout << "muses_table: Warning! Index[" << d << "] overflows table shape!!!" << std::endl;
            return {d,index[d]};
        }
    }
    return {0,-1};
}


// check if entry is filled
bool muses_table::is_filled(size_t order)
{
    size_t pos = ordered[order];
    return filled[pos];

}
bool muses_table::is_filled(std::vector<size_t> index)
{
    validate_index(index);
    size_t pos = indices_to_pos[index];
    return filled[pos];
}

// function to get number of elements from the shape (size along each dimension)
size_t size_from_shape(std::vector<size_t> shape)
{
    size_t size = 1;
    for(auto l : shape)
    {
        size *= l;
    }
    return size;
}


