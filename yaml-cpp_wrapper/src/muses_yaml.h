// #include "yaml_wrap.h"
#include "yaml-cpp/yaml.h"
#include <iostream>
# define LISTALLOC_CHUNKLENGTH 1000

// // struct for table entries
// struct muses_entry
// {
//     // indices for organizing
//     std::vector <size_t> indices;
//     // different quantities
//     std::vector <double> quantities;
    
//     // constructors
//     muses_entry(size_t index, std::vector <double> quantities);
//     muses_entry(std::vector <size_t> indices, std::vector <double> quantities);


// };

// struct for storing table metadata
struct muses_metadata
{
    // metadata and misc
    std::string type;
    std::string version;       
    std::string title; 
    std::string description;

    // constructor
    muses_metadata(std::string type="muses_table", std::string version="beta:0.0.1", std::string title = "muses_table", std::string description = "");
    // print metadata in csv format
    void print(std::ostream &out = std::cout) const;
    // get it in YAML
    YAML::Node get_yaml() const;
};

// struct with table dimensions, columns, keys and units
struct muses_header
{
    // number of columns
    size_t ncols;
    // keys of each column
    std::vector <std::string> keys;
    // convert from key to column
    std::map <std::string, size_t> key_to_col;
    // units of each column
    std::vector <std::string> units;        
    // description of each column
    std::vector <std::string> descriptions;
    // dimension of the index (not necessarily the number of columns)
    size_t index_dimension;
    // shape: how many entries per index
    std::vector<size_t> shape;
    // size of whole thing alltogether (number of entries)
    size_t size=0;

    // constructor
    muses_header(std::vector <std::string> keys, std::vector<size_t> shape);
    muses_header(std::vector <std::string> keys, std::vector <std::string> units, std::vector<size_t> shape);
    muses_header(std::vector <std::string> keys, std::vector <std::string> units, std::vector <std::string> descriptions, std::vector<size_t> shape);

    // print header in csv format
    void print(std::ostream &out = std::cout, std::string sep = "\t") const;
    // get it in YAML
    YAML::Node get_yaml() const;
    // // check if dimensions were specified
    // bool has_dimensions();
    // validate if a key is there
    int validate_key(std::string key);
    
};


// Class for table in MUSES format
class muses_table
{
    private:
        // the actual entry 
        std::vector <std::vector <double>> entries;

        // organization: ordering and indexing
        // when adding elements, one provides indices to position it in each row
        // which index is associated with each column is up to the user.
        // one might index things independent of columns, although this is not likely to be heklpful
        // entries in reading order
        std::vector <size_t> ordered;
        // if each row has been filled, by position (any column counts)
        std::vector <bool> filled;
        // indices of each entry  
        std::vector <std::vector <size_t>> indices;
        // mapping indices to position in list
        std::map <std::vector<size_t>, size_t> indices_to_pos;

    public:
        // metadata and misc
        muses_metadata metadata;
        // table header
        muses_header header;
        
        // default constructor necessary for yaml-cpp integration
        muses_table();
        // default copy constructor to solve gcc warning (see https://github.com/oneapi-src/oneTBB/issues/161)
        muses_table( const muses_table& rhs ) = default;
        // muses_table(muses_table& other);
        // constructor not filling table with anything
        muses_table(muses_metadata metadata, muses_header header);
        // constructor filling table with a default value
        muses_table(muses_metadata metadata, muses_header header, double default_val);

        // writing
        // push back (index, vals)  
        void push_back(std::vector <size_t> index, std::vector<double> entry);
        // automatic indexing
        void push_back(std::vector<double> entry);
        // insert
        // void insert(size_t pos, std::vector <size_t> index, std::vector<double> entry);
        // set entry by order
        void set_entry(size_t order, std::vector<double> entry);
        void set_entry(size_t order, std::string key, double value);
        void set_entry(size_t order, size_t col, double value);
        // set entry by index
        void set_entry(std::vector <size_t> index, std::vector<double> entry);
        void set_entry(std::vector <size_t> index, std::string key, double value);
        void set_entry(std::vector <size_t> index, size_t col, double value);
        // reading
        // get size
        size_t size();
        // get shape
        std::vector<size_t> shape();
        // get entry by order
        std::vector<double> get_entry(size_t pos);
        double get_entry(size_t order, std::string key);
        double get_entry(size_t order, size_t col);
        // get entry by index
        std::vector<double> get_entry(std::vector <size_t> index);
        double get_entry(std::vector <size_t> index, std::string key);
        double get_entry(std::vector <size_t> index, size_t col);
        // // order by key
        // void order_by_key(std::string key, bool ascending = true);
        // order by index, with priority to last one in increasing priority order. default: ascending order
        void order_by_index(std::vector<size_t> priority_order, bool ascending = true);
        // check if entry is valid
        int validate_entry(std::vector<double> entry);
        // check if indices are valid
        int validate_index(std::vector<size_t> index);
        // check if index exists
        bool index_exists(std::vector<size_t> index);
        // check if there is overflow of table dimension
        std::pair<size_t,size_t> shape_overflow(std::vector<size_t> index);
        
        // check if entry is filled
        bool is_filled(size_t order);
        bool is_filled(std::vector<size_t> index);
    
        // print table in csv format
	    void print(std::ostream &out = std::cout, std::string sep = "\t", bool indices = false) const;
        // save table in text format and return yaml metadata
        void save(YAML::Node &yaml_out, std::string filename, std::string sep = "\t") const;

        // assignment overload necessary for yaml-cpp integration
        muses_table & operator=(const muses_table& other);

        // // assignment overload for yaml_table convertion
        // muses_table& operator=(const yaml_table& other);
        // operator (order)
        std::vector<double> operator() (size_t order);
        // operator (index)
        std::vector<double> operator() (std::vector <size_t> index);

        friend YAML::convert<muses_table>; 
};


// function to get number of elements from the shape (size along each dimension)
size_t size_from_shape(std::vector<size_t> shape);

// ##################################################
// #   Legacy template parsing  for yaml-cpp        #
// ##################################################

namespace YAML {
template<>
struct convert<muses_table> 
{
  static Node encode(const muses_table& table) 
  {
    // where everything will go
    Node node;
    // metadata
    node["type"] = table.metadata.type;
    node["version"] = table.metadata.version;
    node["title"] = table.metadata.title;
    node["description"] = table.metadata.description;

    node["shape"] = table.header.shape;
    node["shape"].SetStyle(EmitterStyle::Flow);

    // do each column
    for(size_t i = 0; i < table.header.ncols; i++)
    {
        Node nodecol;
        auto key = table.header.keys[i];
        nodecol["unit"] = table.header.units[i];
        nodecol["description"] = table.header.descriptions[i];
        
        nodecol["value"] = YAML::Load("[]");
        // prepare each entry, in order
        for(size_t ientry =0; ientry < table.ordered.size(); ientry++)
        {
            auto pos = table.ordered[ientry];
            auto entry = table.entries[pos];
            // check if filled
            if(!table.filled[pos]) 
            {
                continue;
            }
            // add entries
            nodecol["value"].push_back(entry[i]);
        }

        node[key] = nodecol;
    }
    
 	return node;
  }

  static bool decode(const Node& node, muses_table& table) 
  {

    std::string title = node["title"].as<std::string>();
    muses_metadata metadata(node["type"].as<std::string>(),node["version"].as<std::string>(),title,node["description"].as<std::string>());
    std::vector <std::vector <double>> entries;
    std::vector <std::string> keys;
    std::vector <std::string> units;
    std::vector <std::string> descs;
    std::vector <std::size_t> shape;
    size_t size = 0;
    size_t nrows = 0;
    size_t ncols = 0;
    for(auto const &subnode : node)
    {
        std::string key = subnode.first.as<std::string>();
        if(key == "title" or key == "type" or key == "version" or key == "description" or key == "shape")
        {
            continue;
        }

        keys.push_back(key);

        std::string unit = subnode.second["unit"].as<std::string>();
        units.push_back(unit);

        std::string desc = subnode.second["description"].as<std::string>();
        descs.push_back(desc);
        
        nrows = subnode.second["value"].size();
    
        std::vector <double> col;
        col.reserve(nrows);
        for(Node element : subnode.second["value"])
		{
			col.push_back(element.as<double>());
		}

        entries.push_back(col);

        ncols ++;
    }

    shape = node["shape"].as<std::vector<size_t>>();
    // size_t dim = shape.size();
    size = size_from_shape(shape); 
    // build table
    muses_header head(keys,units,descs,shape);

    muses_table tableout(metadata, head);
    table = tableout;


    // keep order
    std::vector <size_t> order;
    order.reserve(size);

    for(size_t irow = 0; irow < nrows; irow++)
    {
        std::vector <double> entry;   
        for(size_t icol = 0; icol < ncols; icol++)
        {
            entry.push_back(entries[icol][irow]);
            // std::cout << icol << "\t" << irow << "\t" << entries[icol][irow] << "\n";
        }

        std::vector <size_t> index;
        size_t remainder = irow;
        // get index
        for(auto d : table.shape())
        {
            size_t n = remainder % d;
            remainder = (remainder - n)/d;
            index.push_back(n);
        }
        // std::cout << irow << "\t" << index[0] << "\t" << index[1] << "\t" << entry[0] << ", " << entry[1] << "\n";

        table.set_entry(index, entry);
        // keep order of entries
        // order.push_back(table.indices_to_pos[index]);
       
    }


    // save order of entries
    // table.order = order;

    
    return true;
  }
};
}